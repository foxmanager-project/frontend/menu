function commonGetURLParam(name, url) {
  try {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  } catch (error) {
    console.error('commonGetURLParam', error);
  }
}


function contagemAberturaFormulario(){
	var pDesc = document.querySelector("head > title").innerHTML;
	var pGuid = "";
	var url = "";
	var contextOrigin = window.location.origin;
	var contextPath = window.location.pathname;
	var contextWebrun = contextPath.match("/webrun");
	var contextWebrunStudio = contextPath.match("/webrunstudio");
	var contextHml = contextPath.match("/fox_hml");

	pDesc = pDesc.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
	console.log(pDesc);

	if(contextWebrunStudio != null){
		url = contextOrigin + "/webrunstudio/wscontagemaberturaformulario.rule?sys=FOX&pdesc=" + pDesc + "&pguid=" + pGuid;
	}else if(contextWebrun != null){
		url = contextOrigin + "/webrun/wscontagemaberturaformulario.rule?sys=FOX&pdesc=" + pDesc + "&pguid=" + pGuid;
	}else if(contextHml != null){
		url = contextOrigin + "/fox_hml/wscontagemaberturaformulario.rule?sys=FOX&pdesc=" + pDesc + "&pguid=" + pGuid;
	}else{
		url = contextOrigin + "/wscontagemaberturaformulario.rule?sys=FOX&pdesc=" + pDesc + "&pguid=" + pGuid;
	}

	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", url, false);
	xhttp.send();

	console.log(xhttp.responseText);
}

contagemAberturaFormulario();