//2018-05-17
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * Classe representando o Agente de Envio de NFe
 * @memberof makerplanet.nfe
 * @class
 */
var NFeAgent = (function () {
    function NFeAgent() {
    }
    /**
     * Verifica se o agente foi corretamente inicializado.
     *
     * Essa função pode ser utilizada também para checar a disponibilidade do Agente Local.
     *
     * @return {Boolean} Promise com true se o Agente foi corretamente inicializado, falso
     *     caso contrário.
     * @memberof makerplanet.nfe.Agent
     */
    NFeAgent.initialize = function () {
        return new Promise(function (resolve, reject) {
            var websocket = new WebSocket(NFeAgent.URL + '/recepcao/');
            websocket.onopen = function () {
                websocket.close();
            };
            websocket.onclose = function (ev) {
                resolve(ev.code != 1006);
            };
            websocket.onerror = function (ev) {
                console.log(ev);
            };
        });
    };
    NFeAgent.call = function (endpoint, param, converter) {
        return new Promise(function (resolve, reject) {
            var websocket = new WebSocket(endpoint);
            websocket.onclose = function (ev) {
                if (ev.code == 1006) {
                    alert("Nao foi possivel conectar ao agente.");
                }
            };
            websocket.onopen = function () {
                websocket.send(JSON.stringify(param));
            };
            websocket.onerror = function (ev) {
                reject(null);
            };
            websocket.onmessage = function (ev) {
                websocket.close();
                try {
                    var result = converter(ev.data);
                    if (result.error) {
                        reject(result);
                    }
                    else {
                        resolve(result);
                    }
                }
                catch (err) {
                    reject(ev.data);
                }
            };
        });
    };
	
	/**
     * Chama o webservice de status do serviço.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros do status do serviço da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta do status do serviço da NFe.
     * @memberof NFeAgent
     * @function
     * @name statusNFe
     */
    NFeAgent.status = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/status/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };	
	
    /**
     * Chama o webservice de recepção de nfe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros da recepção da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta da recepção da NFe.
     * @memberof NFeAgent
     * @function
     * @name recepcaoNFe
     */
    NFeAgent.recepcao = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/recepcao/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };
	
	/**
     * Chama o webservice de retorno da recepção do serviço.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros do retorno da recepção da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta do retorno da recepção do serviço da NFe.
     * @memberof NFeAgent
     * @function
     * @name retRecepcaoNFe
     */
    NFeAgent.retRecepcao = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/retRecepcao/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };
	
	/**
     * Chama o webservice de consulta de protocolo da NFe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros da consulta de protocolo da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta da consulta de protocolo da NFe.
     * @memberof NFeAgent
     * @function
     * @name consultaProtocoloNFe
     */
    NFeAgent.protocolo = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/protocolo/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };	
	
	/**
     * Chama o webservice de consulta de cadastro da NFe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros da consulta de cadastro da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta da consulta de cadastro da NFe.
     * @memberof NFeAgent
     * @function
     * @name consultaCadastroNFe
     */
    NFeAgent.cadastro = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/cadastro/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };		
	
	/**
     * Chama o webservice de inutilização da NFe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros da inutilização da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta da inutilização da NFe.
     * @memberof NFeAgent
     * @function
     * @name InutilizaçãoNFe
     */
    NFeAgent.inutilizacao = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/inutilizacao/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };		
	
	/**
     * Chama o webservice do evento de cancelamento da NFe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros do evento de cancelamento da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta do evento de cancelamento da NFe.
     * @memberof NFeAgent
     * @function
     * @name eventoCancelamentoNFe
     */
    NFeAgent.cancelamento = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/cancelamento/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };	
	
	/**
     * Chama o webservice do evento de carta de correção da NFe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros do evento de carta de correção da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta do evento de carta de correção da NFe.
     * @memberof NFeAgent
     * @function
     * @name eventoCartaCorreçãoNFe
     */
    NFeAgent.cce = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/cce/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };		
	
	/**
     * Chama o webservice do evento EPEC da NFe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros do evento EPEC da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta do evento EPEC da NFe.
     * @memberof NFeAgent
     * @function
     * @name eventoEPECNFe
     */
    NFeAgent.epec = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/epec/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };	
	
	/**
     * Chama o webservice do evento de confirmação de recebimento da NFe.
     *
     * @param param {NFeAgent.NFeParam} Parâmetros do evento de confirmação de recebimento da NFe.
     * @returns {NFeAgent.ListDevicesResult} Promise contendo resposta do evento de confirmação de recebimento da NFe.
     * @memberof NFeAgent
     * @function
     * @name eventoConfirmaçãoRecebimentoNFe
     */
    NFeAgent.confRecbto = function (param) {
        return NFeAgent.call(NFeAgent.URL + '/confRecbto/', param, function (data) {
            var result = new NFeAgent.MapResult();
			result.copyInto(JSON.parse(data));		
            return result;
        });
    };		
    NFeAgent.URL = 'wss://wss.makerplanet.com:39095';
    return NFeAgent;
}());
/**
 * Classe representando o modelo base para toda a API
 * @memberof makerplanet.signing
 * @class
 * @abstract
 */
var BaseModel = (function () {
    function BaseModel() {
    }
    BaseModel.prototype.toJSON = function () {
        var y = {};
        for (var i in this) {
            if (!this.hasOwnProperty(i)) {
                // weird as it might seem, this actually does the trick! - adds parent
                // property to self
                y[i] = this[i];
            }
        }
        return y;
    };
    BaseModel.prototype.copyInto = function (data) {
        for (var i in this) {
            if (data.hasOwnProperty(i)) {
                // weird as it might seem, this actually does the trick! - adds parent
                // property to self
                this[i] = data[i];
            }
        }
    };
    return BaseModel;
}());
/**
 * Classe representando os parâmetros comuns a todos os comandos.
 * @memberof makerplanet.signing
 * @class
 * @abstract
 * @extends {BaseModel}
 * @property {string} apiKey Chave da API
 */
var AbstractCommandParam = (function (_super) {
    __extends(AbstractCommandParam, _super);
    function AbstractCommandParam() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(AbstractCommandParam.prototype, "apiKey", {
        get: function () {
            return this._apiKey;
        },
        set: function (value) {
            this._apiKey = value;
        },
        enumerable: true,
        configurable: true
    });
    return AbstractCommandParam;
}(BaseModel));
/**
 * Classe representando as respostas comuns a todos os comandos.
 * @memberof makerplanet.nfe
 * @class
 * @abstract
 * @extends {BaseModel}
 * @property {Boolean} error Erro ocorreu?
 * @property {string} errorMessage Mensagem de erro
 * @property {Boolean} success Sucesso?
 */
var AbstractCommandResult = (function (_super) {
    __extends(AbstractCommandResult, _super);
    function AbstractCommandResult() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(AbstractCommandResult.prototype, "error", {
        get: function () {
            return this._error;
        },
        set: function (value) {
            this._error = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AbstractCommandResult.prototype, "errorMessage", {
        get: function () {
            return this._errorMessage;
        },
        set: function (value) {
            this._errorMessage = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AbstractCommandResult.prototype, "success", {
        get: function () {
            return this._success;
        },
        set: function (value) {
            this._success = value;
        },
        enumerable: true,
        configurable: true
    });
    return AbstractCommandResult;
}(BaseModel));
var NFeAgent;
(function (NFeAgent) {
    /**
     * Parâmetros do comando {@link NFeAgent#recepcaoNFe}
     * @memberof NFeAgent
     * @class
     * @extends {AbstractCommandParam}
     */
    var NFeParam = (function (_super) {
        __extends(NFeParam, _super);
        function NFeParam() {
            _super.apply(this, arguments);
        }

			Object.defineProperty(NFeParam.prototype, "mapa", {
            get: function () {
                return this._mapa;
            },
            set: function (value) {
                this._mapa = value;
            },
            enumerable: true,
            configurable: true
        });
        return NFeParam;
    }(AbstractCommandParam));
    NFeAgent.NFeParam = NFeParam;
    /**
     * Resposta do comando {@link NFeAgent#recepcaoNFe}
     * @memberof NFeAgent
     * @class
     * @property {Array<string>} Mapa contendo resposta do envio da NFe.
     * @extends {AbstractCommandResult}
     */
    var MapResult = (function (_super) {
        __extends(MapResult, _super);
        function MapResult() {
            _super.apply(this, arguments);
        }
        Object.defineProperty(MapResult.prototype, "resposta", {
            get: function () {
                return this._resposta;
            },
            set: function (value) {
                this._resposta = value;
            },
            enumerable: true,
            configurable: true
        });
        return MapResult;
    }(AbstractCommandResult));
    NFeAgent.MapResult = MapResult;
})(NFeAgent || (NFeAgent = {}));
//# sourceMappingURL=makerplanet-scanning.js.map