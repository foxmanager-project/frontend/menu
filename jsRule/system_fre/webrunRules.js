
function AbasMenuCollapseOpcoesDoUsuario(parent, sys, formID) {
  this.ruleName = 'Abas - Menu Collapse Op��es do Usu�rio';
  this.functionName = 'AbasMenuCollapseOpcoesDoUsuario';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

AbasMenuCollapseOpcoesDoUsuario.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Abas - Menu Collapse Op��es do Usu�rio"
 * @author master
 * @since 02/03/2020 19:48:06
 */
AbasMenuCollapseOpcoesDoUsuario.prototype.run = function() {
  document.ruleNameForException = 'Abas - Menu Collapse Op��es do Usu�rio';
  this.context = new Array();


  /**
   * Menu Collapse (remove classe navbar-expand-sm)
   */
  ebfExecuteCustomJSFunction.call(this, ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetElementById.call(this, 'iconsPrincipal'), 'classList'), 'remove', ebfListParamsCreate.call(this, 'navbar-expand-sm'));

  /**
   * Fim
   */
  return null;

}

function runAbasMenuCollapseOpcoesDoUsuario(parent, sys, formID, params) {
  var rule = new AbasMenuCollapseOpcoesDoUsuario(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateAbrirContainerDeAtalhos(parent, sys, formID) {
  this.ruleName = 'Template - Abrir Container de Atalhos';
  this.functionName = 'TemplateAbrirContainerDeAtalhos';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateAbrirContainerDeAtalhos.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Abrir Container de Atalhos"
 * @author master
 * @since 12/03/2020 11:50:46
 */
TemplateAbrirContainerDeAtalhos.prototype.run = function() {
  document.ruleNameForException = 'Template - Abrir Container de Atalhos';
  this.context = new Array();

  // Vari�veis
  this.context['Container Atalhos'] = null;

  this.context['Tamanho da Lista'] = '';

  this.context['Botao Atalhos'] = null;

  this.context['backdrop'] = null;

  this.context['componente em foco'] = '';


  /**
   * Obter Container de Atalhos
   */
  this.context['Container Atalhos'] = ebfHtmlGetElementById.call(this, 'atalhosListaContainer');

  /**
   * Obter Tamanho da Lista de Atalhos
   */
  this.context['Tamanho da Lista'] = ebfListLength.call(this, ebfHtmlChildNodes.call(this, this.context['Container Atalhos']));

  /**
   * Obter Botao Atalhos
   */
  this.context['Botao Atalhos'] = ebfGetElementFromList.call(this, ebfHtmlGetElementsByTagName.call(this, 'button', ebfHtmlGetMakerElementById.call(this, 'atalhosFAB')), parseInt(1));

  /**
   * Maior que 0
   */
  if (parseBoolean(isGreater.call(this, this.context['Tamanho da Lista'], parseInt(0)))) {
      
    /**
     * Definir atributo
     */
    ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'data-toggle', 'collapse');

    /**
     * Obter Botao Atalho
     */
    this.context['Botao Atalhos'] = ebfGetElementFromList.call(this, ebfHtmlGetElementsByTagName.call(this, 'button', ebfHtmlGetMakerElementById.call(this, 'atalhosFAB')), parseInt(1));

    /**
     * Obter Backdrop
     */
    this.context['backdrop'] = ebfGetElementFromList.call(this, ebfHtmlGetElementByClassName.call(this, 'backdrop-shortcut', ebfHtmlGetBodyElement.call(this)), parseInt(1));

    /**
     * Lista de Atalho Aberta ?
     */
    if (parseBoolean(isEqual.call(this, ebfHtmlGetAttribute.call(this, this.context['Botao Atalhos'], 'aria-expanded'), 'false'))) {
        
      /**
       * Mostra  Backdrop
       */
      ebfHtmlSetAttribute.call(this, this.context['backdrop'], 'class', 'backdrop-shortcut show');

      return this.FlowEnd1();

    } else {

      /**
       * Oculta  Backdrop
       */
      ebfHtmlSetAttribute.call(this, this.context['backdrop'], 'class', 'backdrop-shortcut');

      return this.FlowEnd1();

    }

  } else {

    /**
     * Definir atributo
     */
    ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'data-toggle', '');

    /**
     * Definir atributo
     */
    ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'aria-expanded', 'false');

    /**
     * Mensagem de Alerta
     */
    ActNewWarningMessage('N�o h� atalhos criados !', null, parseInt(2), 'DB');

    return this.FlowEnd1();

  }

}

TemplateAbrirContainerDeAtalhos.prototype.FlowEnd1 = function() {

    /**
     * Fim
     */
    return null;
  }


function runTemplateAbrirContainerDeAtalhos(parent, sys, formID, params) {
  var rule = new TemplateAbrirContainerDeAtalhos(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateAbrirFormularioModoGerente(parent, sys, formID) {
  this.ruleName = 'Template - Abrir Formul�rio Modo Gerente';
  this.functionName = 'TemplateAbrirFormularioModoGerente';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateAbrirFormularioModoGerente.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Abrir Formul�rio Modo Gerente"
 * @param Formulario equivale � vari�vel this.context['Formulario']<br/>
 * @author master
 * @since 01/11/2019 10:42:18
 */
TemplateAbrirFormularioModoGerente.prototype.run = function() {
  document.ruleNameForException = 'Template - Abrir Formul�rio Modo Gerente';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Formulario'] = this.checkType(arguments[0], 'Letras');


  /**
   * Obter valor da Lista de Sistemas
   */
  this.context['Formulario'] = ebfFormGetComponentValue.call(this, null, 'ListaSistemas');

  /**
   * Formul�rio Nulo Ou Vazio ?
   */
  if (parseBoolean(isNullOrEmpty.call(this, this.context['Formulario']))) {
      
    /**
     * Mensagem de Alerta
     */
    ActNewWarningMessage(null, 'Selecione um formul�rio!', null, null);

    return this.FlowEnd1();

  } else {

    /**
     * Abrir Formul�rio
     */
    ebfFormOpenForm.call(this, this.context['Formulario']);

    return this.FlowEnd1();

  }

}

TemplateAbrirFormularioModoGerente.prototype.FlowEnd1 = function() {

    /**
     * Fim
     */
    return null;
  }


function runTemplateAbrirFormularioModoGerente(parent, sys, formID, params) {
  var rule = new TemplateAbrirFormularioModoGerente(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateAdicionarAtalhoAoFabContainer(parent, sys, formID) {
  this.ruleName = 'Template - Adicionar Atalho ao FAB Container';
  this.functionName = 'TemplateAdicionarAtalhoAoFabContainer';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateAdicionarAtalhoAoFabContainer.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Adicionar Atalho ao FAB Container"
 * @param Atalho equivale � vari�vel this.context['Atalho']<br/>
 * @author master
 * @since 06/03/2020 10:32:49
 */
TemplateAdicionarAtalhoAoFabContainer.prototype.run = function() {
  document.ruleNameForException = 'Template - Adicionar Atalho ao FAB Container';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Atalho'] = this.checkType(arguments[0], 'Variante');

  // Vari�veis
  this.context['Elementos Texto'] = null;


  /**
   * (remove a Classe position-absolute)
   */
  ebfExecuteCustomJSFunction.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Atalho'], 'classList'), 'remove', ebfListParamsCreate.call(this, 'position-absolute'));

  /**
   * Anexar Atalhos ao Container FAB
   */
  ebfHtmlAppendElementAt.call(this, ebfHtmlGetElementById.call(this, 'atalhosListaContainer'), this.context['Atalho']);

  /**
   * Obter elemento do texto do atalho
   */
  this.context['Elementos Texto'] = ebfHtmlGetElementByClassName.call(this, 'menu-item-text', this.context['Atalho']);

  /**
   * Associar evento dblclick
   */
  ebfHtmlAttachFlowEvent.call(this, this.context['Atalho'], 'ondblclick', 'Template - Ao Clicar no Atalho', ebfListParamsCreate.call(this, null, null, (isNullOrEmpty.call(this, this.context['Elementos Texto']) ? this.context['Atalho'] : ebfGetElementFromList.call(this, this.context['Elementos Texto'], parseInt(1)))), false);

  /**
   * Associar evento keydown
   */
  ebfHtmlAttachFlowEvent.call(this, this.context['Atalho'], 'onkeydown', 'Template - Ao Pressionar uma Tecla', null, true);

  /**
   * Fim
   */
  return null;

}

function runTemplateAdicionarAtalhoAoFabContainer(parent, sys, formID, params) {
  var rule = new TemplateAdicionarAtalhoAoFabContainer(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateAoClicarNoAtalho(parent, sys, formID) {
  this.ruleName = 'Template - Ao Clicar no Atalho';
  this.functionName = 'TemplateAoClicarNoAtalho';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateAoClicarNoAtalho.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Ao Clicar no Atalho"
 * @param Tipo de Evento equivale � vari�vel this.context['Tipo de Evento']<br/>
 * @param Parametros equivale � vari�vel this.context['Parametros']<br/>
 * @param Elemento Nome Atalho equivale � vari�vel this.context['Elemento Nome Atalho']<br/>
 * @author master
 * @since 12/03/2020 12:04:52
 */
TemplateAoClicarNoAtalho.prototype.run = function() {
  document.ruleNameForException = 'Template - Ao Clicar no Atalho';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Tipo de Evento'] = this.checkType(arguments[0], 'Letras');

  this.context['Parametros'] = this.checkType(arguments[1], 'Variante');

  this.context['Elemento Nome Atalho'] = this.checkType(arguments[2], 'Variante');

  // Vari�veis
  this.context['backdrop'] = null;

  this.context['Botao Atalhos'] = null;

  this.context['Aba do Menu'] = null;

  this.context['Objeto Atalhos'] = null;

  this.context['ContainerAtalhos'] = null;


  /**
   * � FORM ?
   */
  if (parseBoolean(isEqual.call(this, this.context['Tipo de Evento'], 'form'))) {
      
    /**
     * Obter aba do menu
     */
    this.context['Aba do Menu'] = ebfGetComponentProperty.call(this, '{DDAE4D3C-A545-4F04-9A20-BABE303A7756}', 'MenuLateralCosmo', 'MenuAbas');

    /**
     * Aba foi definida?
     */
    if (parseBoolean(oprNot.call(this, isNullOrEmpty.call(this, this.context['Aba do Menu'])))) {
        
      /**
       * Adicionar formul�rio na aba
       */
      new AbaAdicionarNovaAba(this, this.getSystem(), this.getForm()).run(this.context['Aba do Menu'], ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(1)), (isNullOrEmpty.call(this, this.context['Elemento Nome Atalho']) ? ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(1)) : ebfHtmlGetInnerHtml.call(this, this.context['Elemento Nome Atalho'])));


      return this.FlowConnector3();

    } else {

      /**
       * Abre form
       */
      ebfFormOpenForm.call(this, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(1)));

      return this.FlowConnector3();

    }

  } else {

    /**
     * � REPORT ?
     */
    if (parseBoolean(isEqual.call(this, this.context['Tipo de Evento'], 'report'))) {
        
      /**
       * Abre report
       */
      ebfOpenReport.call(this, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(1)), null, null, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(2)));

      return this.FlowConnector2();

    } else {

      /**
       * � ACTION ?
       */
      if (parseBoolean(isEqual.call(this, this.context['Tipo de Evento'], 'action'))) {
          
        /**
         * Executa a��o
         */
        ebfActionExecute.call(this, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(1)));

        return this.FlowConnector1();

      } else {

        /**
         * � FLOW ?
         */
        if (parseBoolean(isEqual.call(this, this.context['Tipo de Evento'], 'flow'))) {
            
          /**
           * Quebrar Texto por ;
           */
          this.context['Parametros'] = ebfSplit.call(this, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(1)), ';');

          /**
           * Executa fluxo
           */
          ebfFlowExecute.call(this, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(1)), (isNull.call(this, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(2))) ? ebfListCreate.call(this) : ebfListParamsCreate.call(this, ebfGetElementFromList.call(this, this.context['Parametros'], parseInt(2)))));

          return this.FlowConnector1();

        } else {

          return this.FlowSubRoutine1();

        }

      }

    }

  }

}

TemplateAoClicarNoAtalho.prototype.FlowSubRoutine1 = function() {

    /**
     * Template - Oculta Backdrop de Atalhos
     */
    new TemplateOcultaBackdropDeAtalhos(this, this.getSystem(), this.getForm()).run();

    /**
     * Fim
     */
    return null;
  }

TemplateAoClicarNoAtalho.prototype.FlowConnector1 = function() {

    return this.FlowConnector2();
  }

TemplateAoClicarNoAtalho.prototype.FlowConnector2 = function() {

    return this.FlowConnector3();
  }

TemplateAoClicarNoAtalho.prototype.FlowConnector3 = function() {

    return this.FlowSubRoutine1();
  }


function runTemplateAoClicarNoAtalho(parent, sys, formID, params) {
  var rule = new TemplateAoClicarNoAtalho(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateAoClicarNoItemDoMenuAcao(parent, sys, formID) {
  this.ruleName = 'Template - Ao Clicar no Item do Menu (A��o)';
  this.functionName = 'TemplateAoClicarNoItemDoMenuAcao';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateAoClicarNoItemDoMenuAcao.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Ao Clicar no Item do Menu (A��o)"
 * @param evento equivale � vari�vel this.context['evento']<br/>
 * @author master
 * @since 12/03/2020 12:02:33
 */
TemplateAoClicarNoItemDoMenuAcao.prototype.run = function() {
  document.ruleNameForException = 'Template - Ao Clicar no Item do Menu (A��o)';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['evento'] = this.checkType(arguments[0], 'Variante');


  /**
   * Elemento � o bot�o?
   */
  if (parseBoolean((isEqual.call(this, ebfHtmlGetAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['evento'], 'target'), 'data-target'), '#icons-content') || isEqual.call(this, ebfHtmlGetAttribute.call(this, ebfHtmlGetParent.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['evento'], 'target')), 'data-target'), '#icons-content')))) {
      
    return this.FlowEnd1();

  } else {

    /**
     * (remove a Classe show do Menu A��es)
     */
    ebfExecuteCustomJSFunction.call(this, ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetElementById.call(this, 'icons-content'), 'classList'), 'remove', ebfListParamsCreate.call(this, 'show'));

    /**
     * Definir atributo
     */
    ebfHtmlSetAttribute.call(this, ebfGetElementFromList.call(this, ebfHtmlGetElementByClassName.call(this, 'navbar-toggler', ebfHtmlGetElementById.call(this, 'icons')), parseInt(1)), 'aria-expanded', 'false');

    return this.FlowEnd1();

  }

}

TemplateAoClicarNoItemDoMenuAcao.prototype.FlowEnd1 = function() {

    /**
     * Fim
     */
    return null;
  }


function runTemplateAoClicarNoItemDoMenuAcao(parent, sys, formID, params) {
  var rule = new TemplateAoClicarNoItemDoMenuAcao(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * Menu - Ao Pressionar uma Tecla
 */
function TemplateAoPressionarUmaTecla(parent, sys, formID) {
  this.ruleName = 'Template - Ao Pressionar uma Tecla';
  this.functionName = 'TemplateAoPressionarUmaTecla';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateAoPressionarUmaTecla.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Ao Pressionar uma Tecla"
 * @param Evento equivale � vari�vel this.context['Evento']<br/>
 * @param Elemento equivale � vari�vel this.context['Elemento']<br/>
 * @author master
 * @since 06/03/2020 10:35:27
 */
TemplateAoPressionarUmaTecla.prototype.run = function() {
  document.ruleNameForException = 'Template - Ao Pressionar uma Tecla';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Evento'] = this.checkType(arguments[0], 'Variante');

  this.context['Elemento'] = this.checkType(arguments[1], 'Variante');

  // Vari�veis
  this.context['backdrop'] = null;

  this.context['Botao Atalhos'] = null;

  this.context['Container Atalhos'] = null;


  /**
   * Testa se evento foi chamando pela tecla DEL
   */
  if (parseBoolean((isEqual.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'which'), '46') || isEqual.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'which'), '46')))) {
      
    /**
     * Obter Botao Atalho
     */
    this.context['Botao Atalhos'] = ebfGetElementFromList.call(this, ebfHtmlGetElementsByTagName.call(this, 'button', ebfHtmlGetMakerElementById.call(this, 'atalhosFAB')), parseInt(1));

    /**
     * Obter Backdrop
     */
    this.context['backdrop'] = ebfGetElementFromList.call(this, ebfHtmlGetElementByClassName.call(this, 'backdrop-shortcut', ebfHtmlGetBodyElement.call(this)), parseInt(1));

    /**
     * Oculta  Backdrop
     */
    ebfHtmlSetAttribute.call(this, this.context['backdrop'], 'class', 'backdrop-shortcut');

    /**
     * Oculta Atalhos Container
     */
    ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'aria-expanded', 'false');

    /**
     * Obter Container de Atalhos
     */
    this.context['Container Atalhos'] = ebfHtmlGetElementById.call(this, 'atalhosListaContainer');

    /**
     * (remove a Classe show do Atalhos Container)
     */
    ebfExecuteCustomJSFunction.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Container Atalhos'], 'classList'), 'remove', ebfListParamsCreate.call(this, 'show'));

    /**
     * Elemento do atalho � nulo?
     */
    if (parseBoolean(isNull.call(this, this.context['Elemento']))) {
        
      return this.FlowEnd1();

    } else {

      /**
       * Excluir atalho
       */
      ebfSetRuleExecutionTime.call(this, 'Menu - Excluir Atalho', ebfListParamsCreate.call(this, this.context['Elemento']), parseInt(1));

      return this.FlowEnd1();

    }

  } else {

    /**
     * Testa se evento foi chamando pela tecla F2
     */
    if (parseBoolean((isEqual.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'which'), '113') || isEqual.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'which'), '113')))) {
        
      /**
       * Elemento do atalho � nulo?
       */
      if (parseBoolean(isNull.call(this, this.context['Elemento']))) {
          
        return this.FlowConnector5();

      } else {

        /**
         * Renomear atalho
         */
        ebfSetRuleExecutionTime.call(this, 'Menu - Renomear Atalho', ebfListParamsCreate.call(this, this.context['Elemento']), parseInt(1));

        return this.FlowConnector1();

      }

    } else {

      return this.FlowConnector5();

    }

  }

}

TemplateAoPressionarUmaTecla.prototype.FlowConnector1 = function() {

    return this.FlowEnd1();
  }

TemplateAoPressionarUmaTecla.prototype.FlowConnector5 = function() {

    return this.FlowConnector1();
  }

TemplateAoPressionarUmaTecla.prototype.FlowEnd1 = function() {

    /**
     * Fim
     */
    return null;
  }


function runTemplateAoPressionarUmaTecla(parent, sys, formID, params) {
  var rule = new TemplateAoPressionarUmaTecla(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * Carregar atalhos no formul�rio
 */
function TemplateCarregarAtalhosNoFormulario(parent, sys, formID) {
  this.ruleName = 'Template - Carregar Atalhos no Formul�rio';
  this.functionName = 'TemplateCarregarAtalhosNoFormulario';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;

  this.translations = new Map();

}

TemplateCarregarAtalhosNoFormulario.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Carregar Atalhos no Formul�rio"
 * @author master
 * @since 04/03/2020 16:46:16
 */
TemplateCarregarAtalhosNoFormulario.prototype.run = function() {
  document.ruleNameForException = 'Template - Carregar Atalhos no Formul�rio';
  this.context = new Array();

  // Vari�veis
  this.context['Objeto Atalhos'] = null;

  this.context['Contador'] = 0;

  this.context['Chaves'] = null;

  this.context['Texto JSON'] = '';

  this.context['Atalho'] = null;

  this.context['Tipo de Evento'] = '';

  this.context['Elementos Texto'] = null;

  this.context['Contador'] = parseInt(1);

  /**
   * Obt�m objeto atalhos
   */
  this.context['Objeto Atalhos'] = ebfGetClientFormVariable.call(this, 'menu_obj_atalhos');

  /**
   * Obt�m chaves do objeto
   */
  this.context['Chaves'] = ebfObjectKeys.call(this, this.context['Objeto Atalhos']);

  /**
   * Ainda existem chaves?
   */
  while (parseBoolean(isMinorOrEqual.call(this, this.context['Contador'], ebfListLength.call(this, this.context['Chaves'])))) {

    /**
     * Cria atalho
     */
    this.context['Atalho'] = ebfHtmlCreateHtmlElement.call(this, 'a', ebfListParamsCreate.call(this, ebfListParamsCreate.call(this, 'id', ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], ebfGetElementFromList.call(this, this.context['Chaves'], this.context['Contador'])), 'id')), ebfListParamsCreate.call(this, 'draggable', 'true'), ebfListParamsCreate.call(this, 'href', '#'), ebfListParamsCreate.call(this, 'class', 'menu-shortcut d-flex flex-column align-items-center justify-content-center')), null);

    /**
     * Definir conte�do
     */
    ebfHtmlInnerHtml.call(this, this.context['Atalho'], ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], ebfGetElementFromList.call(this, this.context['Chaves'], this.context['Contador'])), 'conteudo'));

    /**
     * Obt�m Tipo de Evento
     */
    this.context['Tipo de Evento'] = ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], ebfGetElementFromList.call(this, this.context['Chaves'], this.context['Contador'])), 'tipoEvento');

    /**
     * Definir posi��o
     */
    ebfHtmlCssDefineStyle.call(this, this.context['Atalho'], 'left', ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], ebfGetElementFromList.call(this, this.context['Chaves'], this.context['Contador'])), 'left'));

    /**
     * Definir posi��o
     */
    ebfHtmlCssDefineStyle.call(this, this.context['Atalho'], 'top', ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], ebfGetElementFromList.call(this, this.context['Chaves'], this.context['Contador'])), 'top'));

    /**
     * Associar evento mousedown
     */
    ebfHtmlAttachFlowEvent.call(this, this.context['Atalho'], 'mousedown', 'Template - Definir Posi��o do Cursor', ebfListParamsCreate.call(this, this.context['Atalho']), true);

    /**
     * Inserir evento drag
     */
    ebfHtmlAttachFlowEvent.call(this, this.context['Atalho'], 'ondrag', 'Template - Permitir movimenta��o', null, true);

    /**
     * Associar evento para mover atalho
     */
    ebfHtmlAttachFlowEvent.call(this, this.context['Atalho'], 'dragend', 'Template - Mover Atalho', ebfListParamsCreate.call(this, this.context['Atalho']), true);

    /**
     * Associar evento keypress
     */
    ebfHtmlAttachFlowEvent.call(this, this.context['Atalho'], 'keydown', 'Template - Ao Pressionar uma Tecla', ebfListParamsCreate.call(this, this.context['Atalho']), true);

    /**
     * Anexar atalho no formul�rio
     */
    ebfHtmlAppendElementAt.call(this, ebfGetTabDivByName.call(this, ebfTranslate.call(this, ebfGetSelectTabStringName.call(this), null)), this.context['Atalho']);

    /**
     * Template - Adicionar Atalho ao FAB Container
     */
    new TemplateAdicionarAtalhoAoFabContainer(this, this.getSystem(), this.getForm()).run(this.context['Atalho']);

    /**
     * Obter elemento do texto do atalho
     */
    this.context['Elementos Texto'] = ebfHtmlGetElementByClassName.call(this, 'menu-item-text', this.context['Atalho']);

    /**
     * Associar evento dblclick
     */
    ebfHtmlAttachFlowEvent.call(this, this.context['Atalho'], 'ondblclick', 'Template - Ao Clicar no Atalho', ebfListParamsCreate.call(this, this.context['Tipo de Evento'], ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], ebfGetElementFromList.call(this, this.context['Chaves'], this.context['Contador'])), 'parametros'), (isNullOrEmpty.call(this, this.context['Elementos Texto']) ? this.context['Atalho'] : ebfGetElementFromList.call(this, this.context['Elementos Texto'], parseInt(1)))), false);

    /**
     * Incrementa contador
     */
    this.context['Contador'] = oprAdd.call(this, this.context['Contador'], parseInt(1));
  }

  /**
   * Fim
   */
  return ebfHtmlGetInnerHtml.call(this, this.context['Atalho']);

}

function runTemplateCarregarAtalhosNoFormulario(parent, sys, formID, params) {
  var rule = new TemplateCarregarAtalhosNoFormulario(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateDefinirAtributosDoContainerAtalhos(parent, sys, formID) {
  this.ruleName = 'Template - Definir Atributos do Container Atalhos';
  this.functionName = 'TemplateDefinirAtributosDoContainerAtalhos';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateDefinirAtributosDoContainerAtalhos.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Definir Atributos do Container Atalhos"
 * @param Container Atalhos equivale � vari�vel this.context['Container Atalhos']<br/>
 * @author master
 * @since 28/01/2020 15:20:37
 */
TemplateDefinirAtributosDoContainerAtalhos.prototype.run = function() {
  document.ruleNameForException = 'Template - Definir Atributos do Container Atalhos';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Container Atalhos'] = this.checkType(arguments[0], 'Letras');

  // Vari�veis
  this.context['Botao Atalhos'] = null;


  /**
   * Obter Botao Atalhos
   */
  this.context['Botao Atalhos'] = ebfGetElementFromList.call(this, ebfHtmlGetElementsByTagName.call(this, 'button', ebfHtmlGetMakerElementById.call(this, 'atalhosFAB')), parseInt(1));

  /**
   * Definir atributo
   */
  ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'data-toggle', 'collapse');

  /**
   * Definir atributo
   */
  ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'data-target', ebfConcat.call(this, '#', ebfTrim.call(this, this.context['Container Atalhos'])));

  /**
   * Definir atributo
   */
  ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'aria-expanded', 'false');

  /**
   * Fim
   */
  return null;

}

function runTemplateDefinirAtributosDoContainerAtalhos(parent, sys, formID, params) {
  var rule = new TemplateDefinirAtributosDoContainerAtalhos(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * Menu - Definir posi��o do cursor
 */
function TemplateDefinirPosicaoDoCursor(parent, sys, formID) {
  this.ruleName = 'Template - Definir Posi��o do Cursor';
  this.functionName = 'TemplateDefinirPosicaoDoCursor';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateDefinirPosicaoDoCursor.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Definir Posi��o do Cursor"
 * @param Evento equivale � vari�vel this.context['Evento']<br/>
 * @param Elemento equivale � vari�vel this.context['Elemento']<br/>
 * @author master
 * @since 01/06/2015 21:47:10
 */
TemplateDefinirPosicaoDoCursor.prototype.run = function() {
  document.ruleNameForException = 'Template - Definir Posi��o do Cursor';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Evento'] = this.checkType(arguments[0], 'Variante');

  this.context['Elemento'] = this.checkType(arguments[1], 'Variante');


  /**
   * Define posi��o X
   */
  ebfSetLocalVariable.call(this, 'menu_posicaoX', oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'screenX'), toLong.call(this, ebfHtmlCssGetStyle.call(this, this.context['Elemento'], 'left'))));

  /**
   * Define posi��o Y
   */
  ebfSetLocalVariable.call(this, 'menu_posicaoY', oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'screenY'), toLong.call(this, ebfHtmlCssGetStyle.call(this, this.context['Elemento'], 'top'))));

  /**
   * Fim
   */
  return null;

}

function runTemplateDefinirPosicaoDoCursor(parent, sys, formID, params) {
  var rule = new TemplateDefinirPosicaoDoCursor(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * Menu - Excluir Atalho
 */
function TemplateExcluirAtalho(parent, sys, formID) {
  this.ruleName = 'Template - Excluir Atalho';
  this.functionName = 'TemplateExcluirAtalho';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateExcluirAtalho.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Excluir Atalho"
 * @param Elemento equivale � vari�vel this.context['Elemento']<br/>
 * @author master
 * @since 06/03/2020 09:51:16
 */
TemplateExcluirAtalho.prototype.run = function() {
  document.ruleNameForException = 'Template - Excluir Atalho';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Elemento'] = this.checkType(arguments[0], 'Variante');

  // Vari�veis
  this.context['Objeto Atalhos'] = null;


  /**
   * Remove elemento
   */
  ebfHtmlRemoveChild.call(this, ebfHtmlGetParent.call(this, this.context['Elemento']), this.context['Elemento']);

  /**
   * Obt�r JSON
   */
  this.context['Objeto Atalhos'] = ebfGetClientFormVariable.call(this, 'menu_obj_atalhos');

  /**
   * Exclui objeto
   */
  ebfDeleteObject.call(this, this.context['Objeto Atalhos'], ebfHtmlGetAttribute.call(this, this.context['Elemento'], 'id'));

  /**
   * Template - Atualizar Atalhos
   */
  executeSyncJavaRule.call(this, this.getSystem(), this.getForm(), 'Template - Atualizar Atalhos', [ebfGetJSONText.call(this, this.context['Objeto Atalhos'], parseInt(1))]);

  /**
   * Fim
   */
  return null;

}

function runTemplateExcluirAtalho(parent, sys, formID, params) {
  var rule = new TemplateExcluirAtalho(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateFormularioDeLoginObterDadosEEfetuarLogin(parent, sys, formID) {
  this.ruleName = 'Template - Formul�rio de Login - Obter Dados e Efetuar Login';
  this.functionName = 'TemplateFormularioDeLoginObterDadosEEfetuarLogin';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateFormularioDeLoginObterDadosEEfetuarLogin.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Formul�rio de Login - Obter Dados e Efetuar Login"
 * @param Login equivale � vari�vel this.context['Login']<br/>
 * @param Senha equivale � vari�vel this.context['Senha']<br/>
 * @param Digital equivale � vari�vel this.context['Digital']<br/>
 * @author master
 * @since 18/12/2019 11:42:17
 */
TemplateFormularioDeLoginObterDadosEEfetuarLogin.prototype.run = function() {
  document.ruleNameForException = 'Template - Formul�rio de Login - Obter Dados e Efetuar Login';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Login'] = this.checkType(arguments[0], 'Letras');

  this.context['Senha'] = this.checkType(arguments[1], 'Letras');

  this.context['Digital'] = this.checkType(arguments[2], 'Inteiro');


  /**
   * Autentica o usu�rio
   */
  ebfAuthUser.call(this, this.context['Login'], this.context['Senha'], null, null);

  /**
   * Fim
   */
  return null;

}

function runTemplateFormularioDeLoginObterDadosEEfetuarLogin(parent, sys, formID, params) {
  var rule = new TemplateFormularioDeLoginObterDadosEEfetuarLogin(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateFormularioLoginAoEntrar(parent, sys, formID) {
  this.ruleName = 'Template - Formul�rio Login - Ao Entrar';
  this.functionName = 'TemplateFormularioLoginAoEntrar';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateFormularioLoginAoEntrar.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Formul�rio Login - Ao Entrar"
 * @author master
 * @since 03/03/2020 09:56:55
 */
TemplateFormularioLoginAoEntrar.prototype.run = function() {
  document.ruleNameForException = 'Template - Formul�rio Login - Ao Entrar';
  this.context = new Array();


  /**
   * Dar Foco no Input Usu�rio
   */
  ebfFormSetFocus.call(this, 'Login');

  /**
   * Fim
   */
  return null;

}

function runTemplateFormularioLoginAoEntrar(parent, sys, formID, params) {
  var rule = new TemplateFormularioLoginAoEntrar(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateFormularioLoginAoLogar(parent, sys, formID) {
  this.ruleName = 'Template - Formul�rio Login - Ao Logar';
  this.functionName = 'TemplateFormularioLoginAoLogar';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateFormularioLoginAoLogar.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Formul�rio Login - Ao Logar"
 * @param Alt equivale � vari�vel this.context['Alt']<br/>
 * @param Ctrl equivale � vari�vel this.context['Ctrl']<br/>
 * @param Shift equivale � vari�vel this.context['Shift']<br/>
 * @param C�digo da Tecla equivale � vari�vel this.context['C�digo da Tecla']<br/>
 * @param Caractere da Tecla equivale � vari�vel this.context['Caractere da Tecla']<br/>
 * @param Login equivale � vari�vel this.context['Login']<br/>
 * @param Senha equivale � vari�vel this.context['Senha']<br/>
 * @author master
 * @since 30/01/2020 09:27:15
 */
TemplateFormularioLoginAoLogar.prototype.run = function() {
  document.ruleNameForException = 'Template - Formul�rio Login - Ao Logar';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Alt'] = this.checkType(arguments[0], 'L�gico');

  this.context['Ctrl'] = this.checkType(arguments[1], 'L�gico');

  this.context['Shift'] = this.checkType(arguments[2], 'L�gico');

  this.context['C�digo da Tecla'] = this.checkType(arguments[3], 'Inteiro');

  this.context['Caractere da Tecla'] = this.checkType(arguments[4], 'Letras');

  this.context['Login'] = this.checkType(arguments[5], 'Letras');

  this.context['Senha'] = this.checkType(arguments[6], 'Letras');

  // Vari�veis
  this.context['Formulario'] = '';


  /**
   * Obter o GUID do Formulario
   */
  this.context['Formulario'] = ebfGetGUIDActualForm.call(this);

  /**
   * Tecla pressionada � ENTER?
   */
  if (parseBoolean((isEqual.call(this, this.context['C�digo da Tecla'], parseInt(13)) || isEqual.call(this, this.context['Caractere da Tecla'], 'Enter')))) {
      
    /**
     * Template - Formul�rio de Login - Obter Dados e Efetuar Login
     */
    new TemplateFormularioDeLoginObterDadosEEfetuarLogin(this, this.getSystem(), this.getForm()).run(ebfFormGetComponentValue.call(this, this.context['Formulario'], this.context['Login']), ebfFormGetComponentValue.call(this, this.context['Formulario'], this.context['Senha']), null);

    return this.FlowEnd1();

  } else {

    return this.FlowEnd1();

  }

}

TemplateFormularioLoginAoLogar.prototype.FlowEnd1 = function() {

    /**
     * Fim
     */
    return null;
  }


function runTemplateFormularioLoginAoLogar(parent, sys, formID, params) {
  var rule = new TemplateFormularioLoginAoLogar(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateMoverAtalho(parent, sys, formID) {
  this.ruleName = 'Template - Mover Atalho';
  this.functionName = 'TemplateMoverAtalho';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateMoverAtalho.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Mover Atalho"
 * @param Evento equivale � vari�vel this.context['Evento']<br/>
 * @param Elemento equivale � vari�vel this.context['Elemento']<br/>
 * @author master
 * @since 01/11/2019 10:34:54
 */
TemplateMoverAtalho.prototype.run = function() {
  document.ruleNameForException = 'Template - Mover Atalho';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Evento'] = this.checkType(arguments[0], 'Variante');

  this.context['Elemento'] = this.checkType(arguments[1], 'Variante');

  // Vari�veis
  this.context['Objeto Atalhos'] = null;

  this.context['Subtra��o X'] = 0;

  this.context['Diferen�a X'] = 0;

  this.context['Diferen�a Y'] = 0;

  this.context['Constante Y do Navegador'] = 0;

  this.context['Afastamento X Chrome'] = 0;

  this.context['Subtra��o X'] = parseInt(0);
  this.context['Afastamento X Chrome'] = parseInt(0);

  /**
   * Obter Diferen�a X
   */
  this.context['Diferen�a X'] = ebfGetLocalVariable.call(this, 'menu_posicaoX');

  /**
   * Obter Diferen�a Y
   */
  this.context['Diferen�a Y'] = ebfGetLocalVariable.call(this, 'menu_posicaoY');

  /**
   * � FireFox?
   */
  if (parseBoolean(toBoolean.call(this, ebfHtmlGetDOMAttribute.call(this, ebfGetActualForm.call(this), 'isFirefox')))) {
      
    /**
     * Define constante Y
     */
    this.context['Constante Y do Navegador'] = parseInt(0);

    return this.FlowDecision1();

  } else {

    /**
     * � Chrome?
     */
    if (parseBoolean(toBoolean.call(this, ebfHtmlGetDOMAttribute.call(this, ebfGetActualForm.call(this), 'isChrome')))) {
        
      /**
       * Define constante Y
       */
      this.context['Constante Y do Navegador'] = parseInt(0);

      return this.FlowExpression11();

    } else {

      /**
       * Define constante Y
       */
      this.context['Constante Y do Navegador'] = parseInt(0);

      return this.FlowExpression11();

    }

  }

}

TemplateMoverAtalho.prototype.FlowExpression11 = function() {

    /**
     * Compensa afastamento para Chrome e IE
     */
    this.context['Afastamento X Chrome'] = ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, ebfGetActualForm.call(this), 'screen'), 'width');

    return this.FlowDecision1();
  }

TemplateMoverAtalho.prototype.FlowDecision1 = function() {

    /**
     * Testa se atalho foi criado no segundo monitor
     */
    if (parseBoolean(isGreater.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'screenX'), ebfHtmlGetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, ebfGetActualForm.call(this), 'screen'), 'width')))) {
        
      /**
       * Subtrai afastamento para Chrome e IE
       */
      this.context['Diferen�a X'] = oprSubtract.call(this, this.context['Diferen�a X'], this.context['Afastamento X Chrome']);

      /**
       * Define left do navegador
       */
      this.context['Subtra��o X'] = oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, ebfGetActualForm.call(this), 'screenLeft'), oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, ebfGetActualForm.call(this), 'screenLeft'), this.context['Afastamento X Chrome']));

      return this.FlowExpression15();

    } else {

      return this.FlowExpression15();

    }
  }

TemplateMoverAtalho.prototype.FlowExpression15 = function() {

    /**
     * Define CSS
     */
    ebfHtmlCssDefineStyle.call(this, this.context['Elemento'], 'left', ebfConcat.call(this, oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'screenX'), this.context['Subtra��o X'], this.context['Diferen�a X']), 'px'));

    /**
     * Define CSS
     */
    ebfHtmlCssDefineStyle.call(this, this.context['Elemento'], 'top', ebfConcat.call(this, oprAdd.call(this, oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'screenY'), this.context['Diferen�a Y']), this.context['Constante Y do Navegador']), 'px'));

    /**
     * Obt�r JSON
     */
    this.context['Objeto Atalhos'] = ebfGetClientFormVariable.call(this, 'menu_obj_atalhos');

    /**
     * Alterar JSON
     */
    ebfHtmlSetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], toString.call(this, ebfHtmlGetAttribute.call(this, this.context['Elemento'], 'id'))), 'left', ebfConcat.call(this, oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'screenX'), this.context['Subtra��o X'], parseInt(-1), this.context['Diferen�a X']), 'px'));

    /**
     * Alterar JSON
     */
    ebfHtmlSetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], ebfHtmlGetAttribute.call(this, this.context['Elemento'], 'id')), 'top', ebfConcat.call(this, oprAdd.call(this, oprSubtract.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'screenY'), this.context['Diferen�a Y']), this.context['Constante Y do Navegador']), 'px'));

    /**
     * Template - Atualizar Atalhos
     */
    executeSyncJavaRule.call(this, this.getSystem(), this.getForm(), 'Template - Atualizar Atalhos', [ebfGetJSONText.call(this, this.context['Objeto Atalhos'], parseInt(2))]);

    /**
     * Fim
     */
    return null;
  }


function runTemplateMoverAtalho(parent, sys, formID, params) {
  var rule = new TemplateMoverAtalho(parent, sys, formID);
  rule.run.apply(rule, params);
}

function TemplateOcultaBackdropDeAtalhos(parent, sys, formID) {
  this.ruleName = 'Template - Oculta Backdrop de Atalhos';
  this.functionName = 'TemplateOcultaBackdropDeAtalhos';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateOcultaBackdropDeAtalhos.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Oculta Backdrop de Atalhos"
 * @author master
 * @since 12/03/2020 12:04:42
 */
TemplateOcultaBackdropDeAtalhos.prototype.run = function() {
  document.ruleNameForException = 'Template - Oculta Backdrop de Atalhos';
  this.context = new Array();

  // Vari�veis
  this.context['backdrop'] = null;

  this.context['Botao Atalhos'] = null;

  this.context['Container Atalhos'] = null;


  /**
   * Obter Botao Atalho
   */
  this.context['Botao Atalhos'] = ebfGetElementFromList.call(this, ebfHtmlGetElementsByTagName.call(this, 'button', ebfHtmlGetMakerElementById.call(this, 'atalhosFAB')), parseInt(1));

  /**
   * Obter Backdrop
   */
  this.context['backdrop'] = ebfGetElementFromList.call(this, ebfHtmlGetElementByClassName.call(this, 'backdrop-shortcut', ebfHtmlGetBodyElement.call(this)), parseInt(1));

  /**
   * Oculta  Backdrop
   */
  ebfHtmlSetAttribute.call(this, this.context['backdrop'], 'class', 'backdrop-shortcut');

  /**
   * Oculta Atalhos Container
   */
  ebfHtmlSetAttribute.call(this, this.context['Botao Atalhos'], 'aria-expanded', 'false');

  /**
   * Obter Container de Atalhos
   */
  this.context['Container Atalhos'] = ebfHtmlGetElementById.call(this, 'atalhosListaContainer');

  /**
   * (remove a Classe show do Atalhos Container)
   */
  ebfExecuteCustomJSFunction.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Container Atalhos'], 'classList'), 'remove', ebfListParamsCreate.call(this, 'show'));

  /**
   * Fim
   */
  return null;

}

function runTemplateOcultaBackdropDeAtalhos(parent, sys, formID, params) {
  var rule = new TemplateOcultaBackdropDeAtalhos(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * @Lista Elementos: Elementos que s�o criados dinamicamente e/ou n�o fazem parte do Controller.<br/>
 * <br/>
 * @Lista Elementos Controller: Elementos que fazem parte do controller (Bot�o, Caixa de Texto, Lista e outros).
 */
function TemplateOcultaOuExibeElemento(parent, sys, formID) {
  this.ruleName = 'Template - Oculta ou Exibe Elemento';
  this.functionName = 'TemplateOcultaOuExibeElemento';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateOcultaOuExibeElemento.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Oculta ou Exibe Elemento"
 * @param Exibir equivale � vari�vel this.context['Exibir']<br/>
 * @param Lista Elementos equivale � vari�vel this.context['Lista Elementos']<br/>
 * @param Lista Elementos Controller equivale � vari�vel this.context['Lista Elementos Controller']<br/>
 * @author master
 * @since 13/03/2020 11:34:57
 */
TemplateOcultaOuExibeElemento.prototype.run = function() {
  document.ruleNameForException = 'Template - Oculta ou Exibe Elemento';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Exibir'] = this.checkType(arguments[0], 'Letras');

  this.context['Lista Elementos'] = this.checkType(arguments[1], 'Variante');

  this.context['Lista Elementos Controller'] = this.checkType(arguments[2], 'Variante');

  // Vari�veis
  this.context['Contador'] = 0;

  this.context['Contador'] = parseInt(1);

  /**
   * Verifica se a lista foi preenchida
   */
  this.context['Lista Elementos'] = (this.context['Lista Elementos'] ? this.context['Lista Elementos'] : ebfListCreate.call(this));

  /**
   * Verifica se a lista controller foi preenchida
   */
  this.context['Lista Elementos Controller'] = (this.context['Lista Elementos Controller'] ? this.context['Lista Elementos Controller'] : ebfListCreate.call(this));

  /**
   * Possui elemento ?
   */
  while (parseBoolean(isMinorOrEqual.call(this, this.context['Contador'], ebfListLength.call(this, this.context['Lista Elementos'])))) {

    /**
     * Exibe/Oculta o elemento
     */
    ebfHtmlCssDefineStyle.call(this, ebfHtmlGetElementById.call(this, ebfGetElementFromList.call(this, this.context['Lista Elementos'], this.context['Contador'])), 'display', (toBoolean.call(this, this.context['Exibir']) ? 'block' : 'none'));

    /**
     * Incrementa o contador
     */
    this.context['Contador'] = oprAdd.call(this, this.context['Contador'], parseInt(1));
  }

  /**
   * Reinicia o contador = 1
   */
  this.context['Contador'] = parseInt(1);

  /**
   * Possui elemento controller ?
   */
  while (parseBoolean(isMinorOrEqual.call(this, this.context['Contador'], ebfListLength.call(this, this.context['Lista Elementos Controller'])))) {

    /**
     * Exibe/Oculta o elemento
     */
    ebfFormSetVisible.call(this, ebfGetElementFromList.call(this, this.context['Lista Elementos Controller'], this.context['Contador']), toBoolean.call(this, this.context['Exibir']));

    /**
     * Incrementa o contador
     */
    this.context['Contador'] = oprAdd.call(this, this.context['Contador'], parseInt(1));
  }

  /**
   * Fim
   */
  return null;

}

function runTemplateOcultaOuExibeElemento(parent, sys, formID, params) {
  var rule = new TemplateOcultaOuExibeElemento(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * Menu - Permitir movimenta��o
 */
function TemplatePermitirMovimentacao(parent, sys, formID) {
  this.ruleName = 'Template - Permitir movimenta��o';
  this.functionName = 'TemplatePermitirMovimentacao';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplatePermitirMovimentacao.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Permitir movimenta��o"
 * @param Evento equivale � vari�vel this.context['Evento']<br/>
 * @author master
 * @since 01/06/2015 21:47:50
 */
TemplatePermitirMovimentacao.prototype.run = function() {
  document.ruleNameForException = 'Template - Permitir movimenta��o';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Evento'] = this.checkType(arguments[0], 'Variante');


  /**
   * Define tipo de drag
   */
  ebfHtmlSetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'dataTransfer'), 'dropEffect', 'move');

  /**
   * Define tipo de drag
   */
  ebfHtmlSetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Evento'], 'dataTransfer'), 'effectAllowed', 'move');

  /**
   * Fim
   */
  return true;

}

function runTemplatePermitirMovimentacao(parent, sys, formID, params) {
  var rule = new TemplatePermitirMovimentacao(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * Menu - Renomear Atalho
 */
function TemplateRenomearAtalho(parent, sys, formID) {
  this.ruleName = 'Template - Renomear Atalho';
  this.functionName = 'TemplateRenomearAtalho';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

TemplateRenomearAtalho.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Template - Renomear Atalho"
 * @param Elemento equivale � vari�vel this.context['Elemento']<br/>
 * @author master
 * @since 01/11/2019 16:17:41
 */
TemplateRenomearAtalho.prototype.run = function() {
  document.ruleNameForException = 'Template - Renomear Atalho';
  this.context = new Array();

  // Par�metros de Entrada
  this.context['Elemento'] = this.checkType(arguments[0], 'Variante');

  // Vari�veis
  this.context['Nome do Atalho'] = '';

  this.context['Objeto Atalhos'] = null;

  this.context['Chave Atalho'] = '';


  /**
   * Obt�m nome do atalho
   */
  this.context['Nome do Atalho'] = ebfPrompt.call(this, 'Digite o nome do atalho:', ebfHtmlGetInnerHtml.call(this, ebfHtmlChildNodes.call(this, ebfGetElementFromList.call(this, this.context['Elemento'], parseInt(2)))));

  /**
   * Mensagem de Alerta
   */
  ActNewWarningMessage(null, this.context['Nome do Atalho'], null, null);

  /**
   * Verifica se retorno � nulo
   */
  if (parseBoolean(isNull.call(this, this.context['Nome do Atalho']))) {
      
    /**
     * Fim
     */
    return null;

  } else {

    /**
     * Alterar nome do atalho
     */
    ebfHtmlInnerHtml.call(this, ebfHtmlChildNodes.call(this, ebfGetElementFromList.call(this, this.context['Elemento'], parseInt(2))), this.context['Nome do Atalho']);

    /**
     * Mensagem de Alerta
     */
    ActNewWarningMessage(null, this.context['Nome do Atalho'], null, null);

    /**
     * Obt�m JSON
     */
    this.context['Objeto Atalhos'] = ebfGetClientFormVariable.call(this, 'menu_obj_atalhos');

    /**
     * Obt�m nome da chave
     */
    this.context['Chave Atalho'] = ebfHtmlGetAttribute.call(this, this.context['Elemento'], 'id');

    /**
     * Modificar JSON
     */
    ebfHtmlSetDOMAttribute.call(this, ebfHtmlGetDOMAttribute.call(this, this.context['Objeto Atalhos'], this.context['Chave Atalho']), 'conteudo', ebfHtmlGetInnerHtml.call(this, this.context['Elemento']));

    /**
     * Template - Atualizar Atalhos
     */
    executeSyncJavaRule.call(this, this.getSystem(), this.getForm(), 'Template - Atualizar Atalhos', [ebfGetJSONText.call(this, this.context['Objeto Atalhos'], parseInt(2))]);

    /**
     * Fim
     */
    return null;

  }

}

function runTemplateRenomearAtalho(parent, sys, formID, params) {
  var rule = new TemplateRenomearAtalho(parent, sys, formID);
  rule.run.apply(rule, params);
}


/**
 * Percorrer Lista
 */
function Tpt2139b88f012a45028a6f309d5391c231(parent, sys, formID) {
  this.ruleName = '__TPT_{2139B88F-012A-4502-8A6F-309D5391C231}';
  this.functionName = 'Tpt2139b88f012a45028a6f309d5391c231';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

Tpt2139b88f012a45028a6f309d5391c231.prototype = new Rule;

/**
 * Esta fun��o executa a regra "__TPT_{2139B88F-012A-4502-8A6F-309D5391C231}"
 * @author master
 * @since 06/06/2016 14:18:41
 */
Tpt2139b88f012a45028a6f309d5391c231.prototype.run = function() {
  document.ruleNameForException = '__TPT_{2139B88F-012A-4502-8A6F-309D5391C231}';
  this.context = new Array();

  // Vari�veis
  this.context['Contador'] = 0;

  this.context['Tamanho da Lista'] = 0;

  this.context['Objeto Lista'] = null;

  this.context['Contador'] = parseInt(1);

  /**
   * Criar Objeto Lista
   */
  this.context['Objeto Lista'] = ebfListParamsCreate.call(this, null, null);

  /**
   * Tamanho da lista
   */
  this.context['Tamanho da Lista'] = ebfListLength.call(this, this.context['Objeto Lista']);

  /**
   * Possui elemento ?
   */
  while (parseBoolean(isMinorOrEqual.call(this, this.context['Contador'], this.context['Tamanho da Lista']))) {

    /**
     * Complemente com a a��o desejada
     */

    /**
     * Incrementa o contador
     */
    this.context['Contador'] = oprAdd.call(this, this.context['Contador'], parseInt(1));
  }

  /**
   * Fim
   */
  return null;

}

function runTpt2139b88f012a45028a6f309d5391c231(parent, sys, formID, params) {
  var rule = new Tpt2139b88f012a45028a6f309d5391c231(parent, sys, formID);
  rule.run.apply(rule, params);
}

function AbasFormularioPrincipalAoEntrar(parent, sys, formID) {
  this.ruleName = 'Abas - Formul�rio Principal - Ao Entrar';
  this.functionName = 'AbasFormularioPrincipalAoEntrar';
  this.parent = parent;
  this.sys = sys;
  this.formID = formID;
  this.translations = new Map();
}

AbasFormularioPrincipalAoEntrar.prototype = new Rule;

/**
 * Esta fun��o executa a regra "Abas - Formul�rio Principal - Ao Entrar"
 * @author master
 * @since 30/06/2020 11:55:21
 */
AbasFormularioPrincipalAoEntrar.prototype.run = function() {
  document.ruleNameForException = 'Abas - Formul�rio Principal - Ao Entrar';
  this.context = new Array();

  // Vari�veis
  this.context['ListaRetorno'] = null;

  this.context['backdrop'] = null;


  /**
   * Template - Formul�rio Principal - Ao Entrar (Servidor)
   */
  this.context['ListaRetorno'] = executeSyncJavaRule.call(this, this.getSystem(), this.getForm(), 'Template - Formul�rio Principal - Ao Entrar (Servidor)');

  /**
   * Criar backdrop
   */
  this.context['backdrop'] = ebfHtmlCreateHtmlElement.call(this, 'div', ebfListParamsCreate.call(this, ebfListParamsCreate.call(this, 'id', 'backdrop-shortcut'), ebfListParamsCreate.call(this, 'class', 'backdrop-shortcut')), ebfHtmlGetBodyElement.call(this));

  /**
   * Associar evento onclick ao Backdrop
   */
  ebfHtmlAttachFlowEvent.call(this, this.context['backdrop'], 'onclick', 'Template - Oculta Backdrop de Atalhos', null, null);

  /**
   * Obter Menu Collapse
   */
  ebfGetElementFromList.call(this, ebfHtmlGetElementByClassName.call(this, 'navbar-toggler', ebfHtmlGetElementById.call(this, 'icons')), parseInt(1));

  /**
   * Associar evento onclick ao Formulario
   */
  ebfHtmlAttachFlowEvent.call(this, ebfHtmlGetBodyElement.call(this), 'onclick', 'Template - Ao Clicar no Item do Menu (A��o)', null, true);

  /**
   * Adm Geral ou do Sistema ?
   */
  if (parseBoolean(toBoolean.call(this, ebfGetElementFromList.call(this, this.context['ListaRetorno'], parseInt(3))))) {
      
    return this.FlowEnd2();

  } else {

    /**
     * Oculta Menu Adm
     */
    new TemplateOcultaOuExibeElemento(this, this.getSystem(), this.getForm()).run('false', null, ebfListParamsCreate.call(this, 'icons', 'IconButtonContainer'));

    return this.FlowEnd2();

  }

}

AbasFormularioPrincipalAoEntrar.prototype.FlowEnd2 = function() {

    /**
     * Fim
     */
    return null;
  }


function runAbasFormularioPrincipalAoEntrar(parent, sys, formID, params) {
  var rule = new AbasFormularioPrincipalAoEntrar(parent, sys, formID);
  rule.run.apply(rule, params);
}
